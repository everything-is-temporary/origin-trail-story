//Trail 1
//->GrandmaTalk
=== Merchant ===
->Priestess

=== Priestess ===
->Aristocrat

=== Aristocrat ===
->Paladin

=== Paladin ===
->Mercenary

=== Mercenary ===
//->IntroSettingOut
-> END

=== IntroSettingOut ===
NAR: The autumn sun hangs over you as you set out, fields of amber stretching out before you. The brisk wind brushes against your cloak and kisses your cheek, a portent of the winter to come.
NAR: Buoyed by grandma’s reassuring smile, you make your way down the well-travelled path...

-> END

//Starting area - Fields and Forest. Golden rolling hills, Old-growth maple, and golden aspen. The colors of fall.
/*
*Event1
->RandEvent_TheRoadLessTraveled
*Event2
->RandEvent_FreshFruits
*Event3
->RandEvent_AlwaysFordTheRiver
*Event4
->RandEvent_WhoAGoodBoy
*Event5
->RandEvent_ForwardingFogs
*Event6
->RandEvent_TrailOfThorns
*Event7
->RandEvent_ALonesomeShrine
*Event8
->RandEvent_FallingLeafLuckyLeaf
*Event9
->RandEvent_NeitherHereNorThere
*Event10
->RandEvent_PatchyPatchwork
*Event11
->RandEvent_ATravellerInNeed
*Event12
->RandEvent_ForWantOfAWheel
*Event13
->RandEvent_Waterfall
*/

//Event 1 (The Road Less Traveled) [TRLT] (-)
=== RandEvent_TheRoadLessTraveled ===
NAR: Due to the recent rains and the movement of countless other caravans, the main road has become full of thick, sticky mud. The wagon's pace slows to a crawl.

*NAR: Slog through the mud, slow as it is
-> TRLTResolutionTime

*NAR: Lay some hay down on the ground to provide stability
-> TRLTResolutionResource

*{Merchant} Merchant: "Actually, I know another way…"
-> TRLTResolutionChar

    = TRLTResolutionTime
    NAR: The animal pulling the wagon strains to drag it forward.
    NAR: (-X Time)
    ->END
    
    = TRLTResolutionResource
    NAR: Laying the hay down helps make the road more dense, allowing your cart to safely traverse the muddy road.
    NAR: (-2 Hay Bales)
    ->END
    
    = TRLTResolutionChar
    NAR: He leads you off the side of the road. It's a little worrying at first, since the trees seem to thicken even further, but after squeezing through a small copse you land on solid ground.
    The wheels roll smoothly against the surprisingly even stone ground. 
    Merchant: Found it by accident when I was traveling this area a while back. Always thought they should've cleared it out and made a road proper…
    Well, nuthin' to do about it now.
    NAR: (+2 Morale All)
    ->END

//Event 2 (Fresh Fruits) [FF] (+) 
=== RandEvent_FreshFruits ===
NAR: Along the way, you see the gleam of red fruit poking out from between the leaves. 

*NAR: Pass on it
-> FFResolutionTime

*NAR: Stop and pick a few
-> FFResolutionResource

*{Paladin} Paladin: "There's an easy way to get a lot…"
-> FFResolutionChar

    = FFResolutionTime
    NAR: As delicious as they look, there's no time for that now. Your group makes excellent time, though, and when you check the map you're actually a little ahead of where you initially expected.
    NAR: (+X Time)
    ->END
    
    = FFResolutionResource
    NAR: You pluck some of the most easily reachable fruit out of the branches. They smell delicious!
    NAR: (+3 Red Fruit)
    ->END
    
    = FFResolutionChar
    NAR: They walk up to the trunk of the tree, tapping around its circumference a bit before smacking it in a certain location. Several bright fruits drop down around you, which you hurriedly help to scoop up.
    Paladin: "When I wanted to avoid my training, I'd go out and wander in the forest.
    ...Master was always upset with me. But I learned a lot about the plants growing there."
    NAR: (+7 Red Fruit, +2 Morale All)
    ->END

//Event 3 (Always Ford the River) [AFtR] (-)
=== RandEvent_AlwaysFordTheRiver ===
NAR: What was once a little creek has now become a fast-flowing river. It doesn't seem too deep here, but it is uncomfortably wide.

*NAR: Travel along the river til you get to a more easily crossable stretch
->AFtRResolutionTime

*NAR: Always ford the river
->AFtRResolutionResource

*{Aristocrat} Aristocrat: "Couldn't we float across?"
->AFtRResolutionChar

    = AFtRResolutionTime
    NAR: It takes some time to get there, but further up the stream the river narrows
    ->END
    
    = AFtRResolutionResource
    NAR: The wagon tips (-2 random resource)
    ->END
    
    = AFtRResolutionChar
    Aristocrat: "Hey, don't look at me like that! I'm being serious, this is something I learned in my classes. If we seal up the spaces between the boards of the wagon with fibers, it should float.
    Or my name isn't Philimp VI, heir to the Ystachio dynasty-"
    NAR: ...It takes a little time to waterproof the wagon, but somehow, it actually works.
    ->END

//note: if we end up having too many aristocrat events i can make this a merchant event instead, i think

//Event 4 (Who’s a Good Boy?) [WaGB] (-/+)
=== RandEvent_WhoAGoodBoy ===
NAR: A dog calls out to you from the side of the road. What’s a dog doing here?

*NAR: Stop and pet the dog
->WaGBResolutionTime

*NAR: Ignore the dog
->WaGBResolutionTime2

*NAR: Feed the dog!
->WaGBResolutionResource

*{Mercenary} Mercenary: “...he may be of use to us.”
->WaGBResolutionChar

    = WaGBResolutionTime
    NAR: You all stop on the road to spend time with the dog. What a good boy! After scratching the dog’s head for a bit, it goes away on its stray, merry way.
    NAR: (-X Time, +1 Morale)
    ->END
    
    = WaGBResolutionTime2
    NAR: There’s no time for dog distractions. As you pass it up, you hear it whimper behind you. You speed up to avoid guilt. Sorry friend!
    NAR: (+X Time, -1 Morale)
    ->END
    
    = WaGBResolutionResource
    NAR: You toss the dog some scraps of food from the wagon. The dog chews through the food ravenously, leaving nothing behind.
    After licking the floor clear, the dog goes into the bushes, and returns with a bone! It hands you the bone before it goes off on its stray, merry way.
    NAR: (-X Food, +1 Dog Bone, +2 Morale)
    ->END
    
    = WaGBResolutionChar
    NAR: The hulking mercenary kneels down to the dog, and with tricks of the hand, caught the dog’s attention and respect. He takes... something from his bag.
    The dog sniffs it, and promptly enters the forest. In a few moments, the hound returns triumphantly with a rabbit in its jaw. Satisfied, the mercenary takes the spoils and pets the dog off on its stray, merry way.
    Mercenary: “If you value your life, you’d best ignore what you saw just now.”
    NAR: (+4 Meat)
    ->END

//Event 5 (Forwarding Fogs) [FoFo] (-)
=== RandEvent_ForwardingFogs ===
NAR: A deep fog rolls over the road. It’s getting progressively harder and harder to see ahead of you...

*NAR: Take it slow
->FoFoResolutionTime

*NAR: Keep going at the same pace
->FoFoResolutionResource

*{Priestess} Priestess: “Fear not, my light will guide our path!”
->FoFoResolutionChar

=== FoFoResolutionTime ===	
NAR: You call for the animals to slow down until the fog passes. While time had been lost, you are relieved to have avoided a reckless accident otherwise.
NAR: (-X Time)
->END

=== FoFoResolutionResource ===
NAR: You continue at the usual pace, when suddenly, the wagon bumps into a tree! Everyone stumbles over, and a few of your goods were knocked over and spoiled. You promptly throw them out before continuing your way.
NAR: (-1 HP everyone, -2 random resource) 
->END

=== FoFoResolutionChar ===
NAR: The priestess whispered an incantation and sparked a candle aflame. The holy flame glows pure white; as she walks forward into the fog, guiding the animals, the fog is quickly burned away. You all manage to make good time and escape the fog.
Priestess: “We would use these candles to present offerings for the moon... because of these, we never had to worry about the heavens hiding behind cloudy nights. Amazing, isn’t it?”
NAR: (+X Time)
->END

//Event 6 (Trail of Thorns) [ToT] (-)
=== RandEvent_TrailOfThorns ===
NAR: The road soon becomes increasingly wild and unkempt, until it leads into an overgrowth of bushes and thorns. Did you take a wrong turn?

*NAR: Turn around
->ToTResolutionTime

*NAR: Cut your way through [“combat” sequence?]
->ToTResolutionResource

*{Aristocrat} Aristocrat: “It will take more than foliage to hinder our way! Engarde!!”
->ToTResolutionChar

=== ToTResolutionTime ===
NAR: You decided that yes, mistakes were made. The wagon turns around, and sure enough, you reached a fork in the road you missed entirely. You correct course and continue on.
NAR: (-X Time) 
->END

=== ToTResolutionResource ===
NAR: The party takes out tools and begins to cut through the thicket. The thorns and twigs leave your tools in poor condition, but in little time the path ahead is cleared. Passing through, you end up on a part of the main road further along than you expected.
NAR: (-X random tool (knives? swords?), +X Time)
->END

=== ToTResolutionChar ===
NAR: Emboldened by the party’s indecision, the aristocrat sprung into action and charged through the bushes. You are unsure if it was tenacity or dullness of senses that protected him, but eventually he manages to clear out the road all alone with his bare hands!
Passing through, you end up on a part of the main road further along than you expected. Thanks, Philimp!
Aristocrat: “Ha... ha... hahahaha! Bear witness to my power, the conqueror of the forest that tramples all against my path, Sir Philimp the Sixth!”
NAR: (-X Aristocrat HP, +3 Morale, +X Time)
->END

//Event 7 (A Lonesome Shrine) [ALS] (+/-)
=== RandEvent_ALonesomeShrine ===
NAR: A lone statue of the Goddess stands at the trailside, immaculate against the undergrowth. At her feet lay a few candles, still burning, and a plate of bread and fresh fruits, unspoiled and untouched. 

*NAR: Continue down the road
->ALSResolutionTime

*NAR: Give an offering
->ALSResolutionResource

*{Paladin} Paladin: "I-if it’s no trouble, I’d like to...offer a prayer..."
->ALSResolutionChar

//(I’m hesitant about this one)
*Grandma: “Her Holiness wouldn’t mind feeding a few hungry mouths, now would she ;)”
->ALSResolutionChar2

=== ALSResolutionTime ===
NAR: You continue on your way, admiring the statue’s beauty as the wagon rolls by.
->END

=== ALSResolutionResource ===
NAR: [player must choose what they offer] You place your offering in front of the statue, and offer a small prayer of thanksgiving
NAR: (-X resource player chooses, +X Party Morale, -X Grandma’s Morale)
->END

=== ALSResolutionChar ===
NAR: The Paladin kneels before the Goddess with their hands raised skyward. They speak gently in a language that brings back memories of the towering cathedral back home.
NAR: After a few minutes, the Paladin rises up and returns to the wagon with a glowing smile.
Paladin: “I prayed for, ah...good travels ahead of us, Goddess willing.”
NAR: Grandma smiles back.
Grandma: “I’m sure the Goddess listened with her utmost attention.”
NAR: (-X Time, +/-Morale?, maybe this could permanently raise ever party member’s luck?)
->END

=== ALSResolutionChar2 ===
NAR: You and Grandma quickly begin to stuff the food into your knapsacks.
Grandma: “Oh, it’s fine, it’s fine! Statues don’t get hungry, now do they?”
NAR: You can’t help but feel a looming presence as you empty the plate and scurry back to the wagon.
{Paladin} NAR: The Paladin stares in disbelief. They want to say something, but can only muster a burning glare.
NAR: (+X Resources, +/-X Morale?, -X Time, perhaps a bigger consequence down the line.)
{Paladin} NAR: (BIIIIG -X Paladin’s Morale)
->END

//Event 8 (Falling Leaf, Lucky Leaf) [FLLL] (+)
=== RandEvent_FallingLeafLuckyLeaf ===
NAR: You begin to zone out as you stare at the stretching path ahead, leaves showering down. You go forward, step, after step, after step--suddenly a leaf sticks to your face, poking your eye.

*NAR: Shake the leaf off your face.
->FLLLResolutionTime

*Grandma: "Haw haw! It must be your lucky day, little one! Take the leaf and make a wish before it blows away!"
->FLLLResolutionChar

=== FLLLResolutionTime ===
NAR: You shake the leaf off with a frustrated grunt. Grandma checks your eye for injury, holding in a chuckle. Luckily, your eye seems fine.
NAR: (nothing changes. Or like, -.00005 health or morale LOL)
->END

=== FLLLResolutionChar ===
NAR: You take the leaf from your face, rubbing your eye.
Grandma: “Now, hold the leaf in front of you,”
NAR: She holds your hands in hers.
Grandma: “Then say this little chant: ‘Falling leaf, lucky leaf, grant my wish on th’ count of three,’ close your eyes, count to three, and make your wish!”
NAR: You say the chant together, close your eyes, and on the count of three, you wish for…
	
*NAR: A quick journey.
->FLLLResolutionCharTwo

*NAR: A safe journey.
->FLLLResolutionCharTwo
	
*NAR: A fun journey.
->FLLLResolutionCharTwo

*NAR: A cake.
->FLLLResolutionCharTwo

=== FLLLResolutionCharTwo ===
NAR: You make your wish, and open your eyes.
Grandma: “Now make sure to keep that close to you, little one. It’s the only way your wish’ll come true.”
NAR: (+1 Leaf, +X Morale)
->END

//Event 9 (Neither Here Nor There) [NHNT] (+/-)
=== RandEvent_NeitherHereNorThere ===
NAR: The trail begins to narrow until it’s barely as wide as the wagon. Suddenly, a cloaked figure...or at least, the shape of one, emerges from the brush and blocks the middle of the trail. Their form seems to waver in the sunlight speckling through the trees.
NAR: A hissing whisper completely surrounds  you.
->NeitherHereNorThereTwo

=== NeitherHereNorThereTwo ===
Ghost@???: “Please…Have…something…to spare…? Anything…” 

*NAR: Ask them to move aside.
->NHNTResolutionNull

*NAR: Apologize, and go around the cloaked figure.
->NHNTResolutionTime

*NAR: Give something to the figure.
->NHNTResolutionResource

*{Priestess} Priestess: "Stay your hand. This lost soul longs not for material objects. Allow me to commune with them."
-> NHNTResolutionChar

=== NHNTResolutionNull ===
NAR: You ask if they could please make way for the wagon, but they simply repeat their plea.
->NeitherHereNorThereTwo

=== NHNTResolutionTime ===
NAR: You decide to maneuver the wagon just off the trail to skirt around the figure. One of the wheels gets caught in the undergrowth, but with some sweat and time it gets worked free. You continue on your way, the shadowy figure staring at you until it disappears over the horizon.
NAR: (-X Time)
->END

=== NHNTResolutionResource ===
NAR: [Player chooses something from inventory] 
//The following is to be rewritten when we know the right syntax	

*NAR: Give weapon
->NHNT_Weapon

*NAR: Give item
->NHNT_item

=== NHNT_Weapon ===
NAR: As soon as the weapon enters the figure’s hand, it gets enveloped in shadow. They cry out in pain.
Ghost@???: “W...why...WHY?”
NAR: The form begins to bulge and morph into something...monstrous. (pretend you now have to fight the strange figure, and win!)
->NHNT_AfterWeapon

=== NHNT_AfterWeapon ===
NAR: The figure begins to shrink down to its normal size.
Ghost@???: “I…’m...sorry...I couldn’t...help myself…”
NAR: With one last breath, they dissolves into the sunlight. The weapon you gave them clatters to the ground.
NAR: (-1 Weapon, then +1. +/-X Morale from battle. +/-X Health from battle. -X Time)
->END

=== NHNT_item ===
NAR: The figure takes the item into their hands.
Ghost@???: “Th...ank...you…”
NAR: They trudge toward the nearby underbrush, then dissipate into a mist carried away in the wind.
NAR: (-X Resource)
->END

=== NHNTResolutionChar ===
NAR: The priestess steps forward, and without hesitation, grasps the figure’s hands in hers. She utters a single word, and a flash of deep azure and gold surround the pair. Then as quickly as it came, the light dissipates.
NAR: The Priestess stands alone, her head bowed. She returns to the wagon, a tired look in her eyes.
Priestess: “They were a traveler, like you and I, lost on the path between two realms… I only pointed them in the right direction.”
NAR: (-tiny bit Priestess’s Health, +X Morale)
->END

//Event 10 (Patchy Patchwork) [PP] (+/-)
=== RandEvent_PatchyPatchwork ===
NAR: Looking in your knapsack, you find there’s a hole where the supplies used to be.

*NAR: Leave it be
-> PPResolutionTime

*Grandma: "Let me take care of that for you..."
-> PPResolutionChar

=== PPResolutionTime ===
NAR: There’s not much you can do, and you sigh as you’ve lost the use of a sack.
NAR: (-1 inventory space, -2 food)
->END

=== PPResolutionChar ===
NAR: A few short minutes later, and grandma returns your knapsack. Not only is the hole sewn up and the bag expanded a little, there’s a cute smiling patch now on the back!
NAR: (+1 inventory space, -2 food)
->END

//Event 11 (A Traveller in Need) [ATiN] (+/-)
=== RandEvent_ATravellerInNeed ===
NAR: Panicked yelling and neighing catches your attention. Up ahead you see a lone wagon encircled by a pack of ravenous wolves.

*NAR: Ignore the wagon’s plight.
-> ATiNResolutionTime

*NAR: Help them! [Initiate combat]
-> ATiNResolutionTime2

*{Mercenary} Mercenary: "I’ll clear the way!"
-> ATiNResolutionChar

*{Paladin} Paladin: "Enough!"
-> ATiNResolutionChar2

=== ATiNResolutionTime ===
NAR: As you walk by, you notice a crate fall out the back and break, various salted meats spilling out onto the ground that the wolves bite into. Taking advantage of the wolves’ distraction, the wagon hurries away--as do you.
NAR: (-0 Time)
->END

=== ATiNResolutionTime2 ===
NAR: After seeing that the last of the wolves were run off, you look towards the wagon and see it’s already gone. One of the crates it was transporting, however, is sitting in its place. Prying it open you find some salted meat, but the wolves seem to have already gotten in and made off with most of it.
After making sure none of it was spoiled, you load what you can into your storage.
NAR: (-2 Time, +1 Food)
->END

=== ATiNResolutionChar ===
NAR: Before you can do anything, the mercenary charges in, weapon raised and howling war chants. So terrifying was the visage that as soon as the wolves saw the mercenary, they immediately scatter.
The wagon driver, breathing a sigh of relief, gives you a box of food he was transporting as a sign of thanks.
NAR: (+ 4 food)
->END

=== ATiNResolutionChar2 ===
NAR: Before you can do anything, the paladin raises their sword and a brilliant light envelopes the scene. Stunned, both the wolves and wagon freeze as the paladin approaches and demands to know what is going on.
The wagon driver explains he is trying to deliver food to the next town, but the paladin convinces him to part with some of the food to feed the clearly starving wolves.
The wagon driver isn’t happy, but the paladin’s stern look dissuades him from speaking and he is soon on his way. In gratitude, the wolves lead your party to a clearing where you find a wrecked cart you can scavenge.
NAR: (+ 1 spare part, +3 cloth?)
->END

//Event 12 (For Want of a Wheel) [FWoaW] (-)
=== RandEvent_ForWantOfAWheel ===
NAR: With a clang, the wagon comes to a juddering halt. Looking back, you see a wheel has cracked. It doesn’t look like it can be salvaged.

*NAR: Wait and hope another party will pass you by and give you a spare wheel.
-> FWoaWResolutionTime

*NAR: Use the spare wheel.
-> FWoaWResolutionResource

* {Merchant} Merchant - "Ain’t that a shame? Oh well, let me get out my kit and let’s try to save it."
-> FWoaWResolutionChar

=== FWoaWResolutionTime ===
NAR: It takes a few hours, but you’re lucky this route is well traveled and it isn’t too long until someone stops and helps you out. You pay them some gold as thanks.
NAR: (-X Time, -X money)
->END

=== FWoaWResolutionResource ===
NAR: You manage to switch out the wheels without too much difficulty and be on your way again.
NAR: (- 1 spare part, -X Time)
->END

=== FWoaWResolutionChar ===
NAR: An hour or so later, the merchant stands back up and shoots a thumbs up at you. When the wagon moves again, everyone is pleasantly surprised at how sturdy the merchant’s fix holds up, and the party makes up for lost time.
NAR: (+1 morale)
->END

//Event 13, which could be used on another route instead (Waterfall) (+/-)
=== RandEvent_Waterfall ===
NAR: The sun burns unbearably hot today, and the animals begin to pant with thirst and exhaustion. Luckily, you begin to hear the sound of rushing water ahead.

*NAR: Find the water source and let the animals rest.
->WaterfallResolutionTime

*NAR: Keep going despite the heat.
->WaterfallResolutionTime2

=== WaterfallResolutionTime ===
NAR: The rushing grows louder as you continue down the path. Then you see it through the trees: a waterfall pouring down in a gentle arch across a mossy cliff face, into a rippling pool of clear water.
NAR: Sunlight glints off the surface, and tiny minnows dart back and forth in the rocks and reeds lining the shore. You unhook the animals’ harnesses and lead them to the poolside. They eagerly lap up their fill of the cool water, then find a shaded tree to rest under.

*NAR: Wait for the animals to fully recover.
-> WaterfallResolutionTimeTwo

*NAR: Harness the now animals and depart.
-> WaterfallResolutionTimeTwo2

*{Aristocrat} Aristocrat: “Hey, everyone! Up here!”
-> WaterfallResolutionTimeTwo3

*{Merchant} Merchant: “This reminds me of a song…”
-> WaterfallResolutionTimeTwo4

=== WaterfallResolutionTimeTwo ===
NAR: You let the animals sleep for a few hours, then continue down the road with renewed energy.
NAR: (-A lot of Time, +A lot of Morale/health)
->END

=== WaterfallResolutionTimeTwo2 ===
NAR: You reharness the animals and continue down the road.
NAR: (-some Time, +Some Morale/health)
->END

=== WaterfallResolutionTimeTwo3 ===
NAR: You look up to see him at the top of the cliff, wearing only his undergarments, standing near the edge of the waterfall. Grandma sighs and rolls her eyes with a smile.
Grandma: “Philimp, ya fool, What are you doing? Get back down here before you hurt that perfect face of yours!”
Aristocrat: “Hah, you may call me foolish, but history will call me courageous!”
NAR: He takes a few steps back. After a moment, he barrels toward the edge screaming at the top of his lungs, bending down to leap up, but hesitates. It’s too late, though, and his momentum carries him over the edge belly first. His scream becomes a cry, then he hits the water with a resounding clap.
NAR: All is silent. Then a chuckle escapes from Grandma’s red face. Everyone breaks into laughter—even Philimp as he swims toward the shore, his chest a bright red from the impact.
Aristocrat: “I’ve...always...wanted...to do that…” he says through laughs and pants.
NAR: After the Aristocrat dries off, you yolk up the animals and head back onto the road.
NAR: (-X Time, +X Morale, -small amount of Philimp’s health maybe lol)
->END

=== WaterfallResolutionTimeTwo4 ===
Merchant: “My grandad used to sing it to me. Here, I’ll teach it to ya’. Listen closely…”
NAR: The Merchant clears his throat, then begins to hum a lilting melody, in a deep melancholy baritone. He repeats it a few times, then turns to you.
Merchant: “Now, keep humming that tune, and I’ll add the words.”
NAR: You hum to the tune as best you can with your small high voice, stumbling here and there, but the Merchant just nods and smiles encouragingly as he hums along. 
NAR: Grandma joins in with a sliding alto  harmony line that sends shivers down your neck. She’s heard this song before. Then, the merchant begins to sing.
Merchant: 	As tired beast must rest to bear its burden,
/So water falls to feed a flowing stream.
/As tired souls must sleep to meet the morrow
/So water flows to meet an endless sea./

/As sunlight fades to silver moonlight’s shadow
/So rain will pour to feed the growing tree.
/As seasons pass from autumn unto winter
/So rain will fall to quench earth’s dying plea./

/And So it falls, it flows, it falls,
/Down river swift and mountain tall
/And So it falls, it flows, it falls
/Far reach its arms into the sea.
/And So it falls, it pours, it falls
/Unto the land eternally
/and So it falls, it pours, it falls
/And makes the land become the sea./

/it falls, it flows, it pours, it falls
/I reach my hand into the sea
/it falls, it flows, it pours, it falls
/To find a peace that cannot be./

/I yearn for peace that cannot be
/Yet time, yet time, falls over me.”

NAR: You and grandma stop humming. The merchant repeats the last line a few more times, and eventually fades off. You stare at the spot where the waterfall meets the pool. “That was lovely, Caelin…” Grandma says, a sad smile creasing her cheeks.
NAR: After a few moments of silence, you get to work harnessing the animals, and set off toward the road once more.
NAR: (-X Time, +Morale/health, +singing skill lol)
->END

=== WaterfallResolutionTime2 ===
NAR: You continue to work the animals down the road under the blazing sun, passing a gushing waterfall along the way. After an hour or so, one of the animals buckles under its own weight, panting heavily. You use up some of your food and water to bring it back to good enough health to continue the journey. (-quite a bit of resources). 
->END
