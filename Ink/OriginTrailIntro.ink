    *CHILD: 	"Grandma, Grandma!"
    -> Grandma

=== Grandma ===

Grandma:	"Yes, my dear?"

    *Child:	"Grandma, tell that story, again!"
    Grandma: 	"That story? Use your words, dear."
    -> TheTowerStory

=== TheTowerStory ===
    *Child:	"The tower story, please!"
    Grandma: 	"Oh, all right then."
    -> PictureBook

=== PictureBook ===
Grandma: 	"A long time ago, before you and me, before the town and the farms, before fresh bread and warm blankets, before most anything at all…"
	"Was a god."

    *[(Picture fades in, page turn)]
    -> PictureBook1

=== PictureBook1 ===

Grandma:	"She enacted in godly business, creating works of art almost beyond description. Shimmering paintings in colors beyond the rainbow, swirling mosaics that tell stories of people and things not of this world, life-like sculptures made of the very stuff of life..."
 	"Of course, she grew tired, doing this end on end, so she stood from her lavish workshop, lined with artworks of peerless beauty, and went to get a snack."

    *[(Page turn)]
    -> PictureBook2

=== PictureBook2 ===
Grandma:	"Gods aren't like you or me, of course. Instead of potato stew, or shepherd's pie, or any other kind of food you or I could eat, gods eat planets."

"She went out to her garden and plucked out a gorgeous little planet, with beautiful, verdant green forests, deep and cool oceans, and soft, gently wafting clouds."
"The dirt was just right, rich but firm. The snowy peaks of mountains, covered in smooth layers of frost..."
"Can you imagine? Just taking a big bite right out of that beautiful little planet."

    *[(Page turn)]
    -> PictureBook3

=== PictureBook3 ===
Grandma:	"The god took it in, and in her kitchen, took a roasting spit and struck it straight through the planet in one swift motion, ready to place it over the fire to cook."

"But at the last moment—she took a closer look at that little planet, and saw how beautiful it was."
"The god, in her infinite love of art, felt her hunger melt away, and instead decided to keep the planet forever more, to look at for inspiration or even just admiration."
"She took it to her treasure room, a place full of glittering jewels and all sorts of wonderful trinkets and things, sparkling in a myriad of colors, and placed it right in the middle where she'd always be able to see it."

    *[(Page turn)]
    -> PictureBook4

=== PictureBook4 ===
Grandma:	"And every day, she'd enter the room with a big, warm light in hand, and walk through and stare at all her treasures."

"That's why the sun comes to the sky every morning, and leaves in the evening. Our lady is passing through the room, giving our little planet a look."
"All those jewels and baubles, you see them at night when the stars shine in the sky."

    *Child:	"And—the tower?"
    -> TheTower

=== TheTower ===

Grandma:	"Yes, yes, dear. That spit the god had left through the planet, it began to get worn down, carved out by the flowing winds and rain."

"The roasting spit was carved into two towers, one each where it jutted out of the earth."

"One day at one of the towers, something changed. All the way from its top, walking down its spiraling staircase, came people."
"From the Tower of Origin, people came, and traveled and journeyed all throughout the land."

"The other tower, the Tower of End—"

    *Child:	"Grandma!"
    
    Grandma:	"Yes, dear?"
    ->TheView

=== TheView ===
    *Child:	"I wanna know about the view! From the top of the tower, where the people came from!"

Grandma:	"Oh, it's gorgeous. The top of the tower is so high, you can look out over the land and see everything—the little towns, all alight with life. The pointy tops of all the mountains."
"Even the clouds float far below. The whole world is a canvas of beautiful colors—the sunset bathing everything in glowing light, the shimmering reflection on the ocean's surface. It's beautiful."
->TheStars

=== TheStars ===
    *Child:	"And the stars?"

NAR: A hearty chuckle rumbles through the air.

Grandma:	"Yes, and the glittering stars so close you could nearly reach up and snatch them out of the treasure room."
"Don't do it though! The goddess will become upset, and who knows, we might end up back on the fire pit!"
->See

=== See ===

NAR: A yawn breaks through the quiet of the night.

    *Child:	"I...wanna see that someday...not the fire...but...the stars..."

    Grandma:	"Me too, dear. Perhaps we'll go off on an adventure, someday."
    ->WhenTheyReturn

=== WhenTheyReturn ===

Child:	"Yeah...when...mama and papa return…"

Grandma:	"...Yeah. I'd like that as well."

    *[Slowly fade to black]
    ->Fading

=== Fading ===

Grandma:	"Sleep well, dear."

    *[Fade to black]
    -> END

=== GrandmaTalk ===
 { Bakery:
 -> SettingOutAdventure
    -else:
    -> Waking
}

=== Waking === //Waking

//Waking Conversation
//Characters: G = Grandma, C = Child
{Waking == 1:

Grandma:	"Wake up, little one."

Child:	"Hmmm?" //[screen/background comes into focus]

NAR: You blink. Wiping the sleep from your eyes you look up and see your Grandma standing above you, a beaming smile on her face.
NAR: As you sit up, you peer around the room and see the familiar interiors of the small cottage you live in. 
NAR: Before you can think too much though, your grandma speaks.

Grandma:	"Would you be a dear and run to the baker to get some bread?"

    + Child:	"Okay…"
    ->Okay
    
    + Child:	"I don’t wanna…"
    ->NotOkay
    
-else:
Grandma: "Haw haw! Come on, little one!"

Grandma: "Get your butt over to the bakery, then come straight back!"
-> END
}
=== NotOkay ===
Grandma:	[stares]

    + Child:	"Okay..."
    ->Okay

=== Okay ===
Grandma:	"Such a good child. Here’s X gold, enough for Y loaves.
You remember where the bakery is, right?"

    + Child:	"Yeah...it has the sign."
    ->NoDirections
    
    + Child:	"No..."
    ->Directions

=== Directions ===

Grandma:	"Just go down the street and look for the sign, sweety. You can’t miss it."
->NoDirections

=== NoDirections ===

Grandma:	"Well, you better get a move on then. Say hello to the baker for me!"
    + NAR: [open the door and go out]
    ->END
    
=== ToBaker ===
//On the Street, Moving Towards Bakery
//Characters: P1 = Person 1, P2 = Person 2, Pr = Preacher, P3 = Person 3
//Note: Horizontal line separates different incidents/NPC threads

NAR: The door shuts behind you and you find yourself in the street, empty save a few passersby. You look around, and wonder why Grandma woke you up so early.
Then your eyes narrow and focus on the midday sun and you realize the time. Huh. No one else had grandmas that woke them up early; they all were still asleep! 
Sighing you make your way down the street towards your destination.

    + NAR: [(Someone runs past character, panicked)]
    ->BackgroundTo

=== BackgroundTo ===
 { Bakery:
        P2:	"Remember how proud we were, seeing them wave the banner as they left?"
    
        P1:	"Humph. Yeah..."
    -else:
        P1:	"No, that can’t be right…"
    
        P2:	"It is. I wish it wasn’t, but it is."
    
        P1:	"...What are we going to do?"
        
    }
->END

=== Priest ===
    { Bakery:
        Pr:	"Stay together! Be with your loved ones! Speak and socialize as you do! Toil as you always have!"
        
        Pr: "Let the Spring Festivities commence with the grandeur it always possesses! Do not squander what…"
    -else:
        Pr:	"All will be right! Trust in your sight! Trust in your neighbors!
        
        Pr: "Look over yonder, see the hills! They are still green, are they not?"
        
        Pr: "Look to your homes! Your families, your livestock, your livelihoods!"
        
        Pr:  "They. Are still. Here! As long as that remains true…"
    }
->END

=== P3 ===
{ Bakery:
    P3:	"...snow and sickness stopped me from goin’ with ‘im..."    
    -else:
    P3:	"My son was there…"
    }
->END

=== Bakery ===

//Bakery
//Characters: CHILD, baker
{Bakery == 1:
NAR: It is not the smell that strikes you first. Or at all.

NAR: As you step through the door and the bell above it gives a cheery little ring, you look out at the display cases full of pastries and other assorted delectables. 
NAR: But the baker seems...distracted. //They don’t seem to be giving off the steam they normally do--and look just a little slumped. Standing amidst the rows of pastries is the baker, a portly man sporting a mane of red hair that usually compliments his wide smiles and radiant attitude when customers enter.
// But he doesn’t seem to have noticed your entrance, and is staring blankly into a wall. Then again his bushy mustache hides most of his face from where you’re standing, so who are you to say where he’s looking?

//[Child interacts with Baker]

    + Child:	"2 loaves of bread, please!"
    ->BakerContact
    -else:
    
    NAR: The baker seems...distracted.
    -> END
    }

=== BakerContact ===

Baker:	"Oh! H-hello there! I uh--didn’t see you there.
What can I do for you?"

    + Child:	"2 loaves of bread!"
    		//- X money
    ->BakerGiveth

=== BakerGiveth ===
Baker:	"Here."
		//+ Y - 1 loaves

    + Child:	"Mister! You forgot a loaf!"
    ->BakerConfuse

=== BakerConfuse ===
Baker:	"Oh, w-what's that? You need another loaf...?
Uh, well, it seems like I've run out of plain bread. How about you take a cake, instead?"

    + Child:	"You sold all your bread already? Wow…"
    ->ChildWonder

=== ChildWonder ===
Baker:	"Pah, no. I just...couldn’t bring myself to make any this morning. I just gave you the last of yesterday’s batch--still good! But…"

    + Child:	"Your bread is always good, even if it isn’t fresh!"
    ->ChildCompliment

=== ChildCompliment ===
Baker:	"Hah! Yes! And..."

NAR: His face falls.

#sad
Baker:	"Actually, take your money back."
		//+ X money
Baker:	"And here, why don’t you take another cake while you’re here."
		//+ 1 cake
Baker:	"Because I didn’t give you enough bread. Have another. For your family, see."
        //+ 1 cake

    + Child:	"Wow! So this is all free?!"

    Baker:	"Yup! All of it."
    ->MissionComplete
    
    + Child:	"Mom and dad went away last winter, remember?"
    
    Baker:	"..."
    ->MissionComplete

    + Child:	"Is something the matter, mister?"
    Baker:	"..."
    ->MissionComplete

=== MissionComplete ===

Baker:	"Well, I hope that’s all you need. Can you carry all that?"

    + Child:	"Yeah. Yeah! It all fits in my backpack!"
    
    Baker:	"Give-Give your family my best."
    ->Best

=== Best ===
    + Child:	"Oh yeah, my grandma says Hi!"
    
    Baker:	"Bye! Spend your days well!"
    ->END

=== FromBaker ===

//On the Street, Moving Back Home
//Characters: P3 = Person 3, Pr = Preacher, P1 = Person 1, P2 = Person 2

NAR: The door shuts behind you and you find yourself in the street, empty save a few passersby. You look around and the street hasn’t changed except for the weird energy you’ve been getting this morning.
NAR: What’s with all the adults? Sighing you make your way down the street towards your destination.
->IntoHouse

=IntoHouse
+ NAR: [Go into the house]
-> SettingOutAdventure

=== SettingOutAdventure ===
{ SettingOutAdventure == 1:
NAR: You run through the door and see Grandma busy at the kitchen table, stuffing various clothes and other small things into her rucksack, mumbling under her breath.

Grandma: "Hmm, let’s see… what else would we need, I feel like I’m forgetting something—"

She quickly looks up and smiles as you bouncing up and down, unable to contain your excitement for the extra goodies from the nice baker man.

Grandma: "Ah good, you’ve returned, and so quickly, too! Have you carried out the quest I gave you, oh brave little hero?"

+ Child: "Yes, ma’am!!"

Grandma: "Excellent! Now, show me what thou carriest in thine backpack of treasures so that I may reward you properly."
->Show
-else:
 Grandma: "Walk over to the signpost at the faaaaar end of the village..."

Grandma: "and interact with it for us to leave the village."
-> END
}

=== Show ===
    + Child: [Show Grandma the baked goodies.]
    ->Results
    
    + Child: "Well…" [Show Grandma the baked goods.]
    ->Results

= Results

NAR: Grandma furrows her brow.

Grandma: "…"

Grandma: "A single loaf, and three cakes? You know this isn’t what I asked you to get—and what’s this?
The money I gave you, as well?! Don’t tell me you forgot to pay the man, again... What happened this time, little one?"

    + Child: "I got it all for free! I promise!"
    ->Promise

= Promise
Grandma: "You promise promise he gave it to you? Free? All of this?"

    + CHILD: "Yes! I promise promise PROMISE!!"
    ->Promise2
    
= Promise2
    Grandma: "Okay, okay, well—he is a good friend, and under these circumstances, it’s understandable, but still..."

    + Child: "The baker man was acting really weird today, and…"
    ->Weird

= Weird
Grandma: "To give all this for free… It’s understandable, given these circumstances, but…"

Grandma: "I just hope he will be okay."

Grandma: "Regardless..." 

NAR: Grandma regains her playful composure.

Grandma:  "Oh, great little hero! You have hereby completed my quest!"

Grandma: And for that, I grant you a treasure most precious, and most sacred..."
Grandma: "...imbued with all the magic that runs through my old veins."

Grandma: "..."

Grandma: "..........."

Grandma: "A cake!"
-> cake
/*Grandma: "It sits upon your bed, at the back of the room. Now, make haste, hero! Once you grab it, come back to the kitchen and I’ll give you one of these delicious cakes~"

Grandma: "Just don’t tell your grandma you’re having sweets for breakfast—"

Grandma: "Oh wait, that’s me!"

//[player walks to bed, and takes the item]

//[Player obtains a wrapped gift]

//[player walks back to Grandma]
->Gift

=== Gift ===
    + Child: "What’s inside?"
    Grandma: "There’s only one way to find out, isn’t there?"
    ->GiftResults

    + Child: "Can I open it yet???"
    Grandma: "I won’t stop you!"
    ->GiftResults
     
    + Child: "Cake please!!"
    Grandma: "Woah, woah, woah, not before you open up your gift!"
    ->GiftResults

=== GiftResults ===
//[Child opens gift]

//[Player obtains book!]

//[Book slides up, taking up the bottom half of the screen and flips open. Pages are currently blank]

Grandma: "I know you’ve asked me time and time again how I write my stories, and how your father wrote his…"

Grandma: "So, I thought it was about time I start teaching you how to write some of your own."

Grandma: "What do you think, little one? Do you like it?"

    + Child: "I love it!"
    Grandma: "I’m glad you do, because you’ll need it for what’s ahead of us."
    ->Tome
    
    + Child: "Cake, please!!!"
    Grandma: "Uh-uh-uh, not until after one more quest!"
    ->Tome

= Tome
Grandma: "Now, let’s break in that tome of yours! I always say, an empty book is a sad book, so let’s make this one happy~"

Grandma: "I want you to look around, and draw what you see. Write down what you hear, and what you feel, what you think others feel."

Grandma: "Like I’ve told you before, a good artist always observes the world around them, and a great one always records it. So get recording, little one!"

//[child begins furiously writing/drawing in the book]

//[book flips to map tab, and a little drawing of grandma’s house appears at the far left]

//[book flips to friends tab, and a drawing of grandma plus her current status and a description of her appear, same for child, whose hunger bar seems half full]

//[then flips to the inventory page, and inventory spot for this book is filled (probably some childish comment about how it’s an inventory slot referring to the very thing the inventory is recorded in)]

Grandma: "Done? Let me take a look…"

NAR: Grandma beams.

Grandma: "Beautiful, little one! I think you’ve finally earned yourself...
...this!"

//[grandma gives child cake]

//[Cake appears in the inventory, with a little comment written beside it: “MY FAVORITE” and stars drawn all around it]

Grandma: "Now, go ahead and eat it!"

//[pic of cake glows, to indicate to players to click on it.]

//[player clicks on cake pic on the inventory page, and drop down menu of actions appears: “info, eat, give, discard” with only “eat” being available for now (of course later on all the options will be available)]

//[player chooses eat, and book automatically flips to friends tab. A prompt shows up saying “who will you feed cake to?” or something like that, and there’s a “Feed” button above each character’s portrait on their page. Player can flip between character pages using the arrows on the bottom corners of the pages. When player hovers cursor over “Feed” button, the cursor becomes an icon of the item (cake, in this case) and the number of that item (1, in this case). Player must click the “Feed” button on the page of the person they want to feed. There is an “X” or cancel button at the top right of the book which the player can click to cancel out of this “eat” action entirely, which makes book flip back to the inventory tab, specifically to the item they were interacting with.]

//[if player chooses grandma:]
Grandma: "Oh no, that’s for you, little one. I already ate. Go ahead, eat the cake!"
-> END

//[if player chooses “Me,” the cake is consumed, and the number on the cursor changes from 1 to 0, and book shows the stat changes immediately. If we had more than 1 cake, player could’ve fed multiple cakes (but we only have 1 this time). Player can’t exit this “feed” interaction state until they hit the “X” button. As stated earlier, once player hits “X” button, book flips back to the item they were interacting with, in the inventory tab.

//That’s generally how I’m thinking the flow of using items might go.]

//[player eats cake] */
=== cake 
Grandma: "I hope it's as delicious as you hoped, my little sugar goblin~"

    + Child: "It’s better than delicious!!"
    ->CakeEat
    
    + Child: "Mhmm!!"
    ->CakeEat
    
    + Child: "It’s not as good as it usually is…"
    ->CakeEat

= CakeEat
Grandma: "Hey, I know you’re my little goblin, but no talking with your mouth full!"

Grandma: "Now! I have one more surprise for you."

Grandma: "..."

Grandma: "It's time for us to go on a journey...

Grandma: "...to the top of the Tower of Origin."

Grandma: "Walk over to the farthest end of the village from here..."

Grandma: "And that should take you to the road."

Grandma: "I'll meet you there."
-> END


//[grandma puts on her rucksack and begins walking out of the house. She waits for player at the door and lets them walk out first, looks around the house one more time, then walks out*]
//->OutsideOnAdventure

= OutsideOnAdventure
//[grandma and child walk toward village gate]

    + Child: "Why is everyone acting so strange today?"
    ->Strange

= Strange
Grandma: "People act strange when strange things happen, my child."

    + Child: "Something strange happened?"
    ->SomethingStrange

= SomethingStrange
Grandma: "…"

    + NAR: [Follow grandma]
    ->VillageGate

= VillageGate

//[grandma and child arrive at village gate]

Grandma: "Now that we’re here..."

NAR: Grandma reaches into her rucksack and pulls a maroon cloak out.

Grandma: "Look, little one. I made this for...your father, when he was young."

Grandma: "He always wore it proudly—not without a little push from me, of course—whenever and wherever he would travel the roads outside the village."

Grandma: "He was a lot like you as a child: ever curious, ever observant, loud in spirit, and louder in voice—"

Grandma: "…"

Grandma: "As all children do, he outgrew this and I put it away, hoping that he could one day give it to his child when they were old enough to travel, and them to their child, and so on, and so on."

Grandma: "And so, little one, I know I’m not your father, but it’s now your turn to don the cloak, like he did before you."

//[Player obtains traveler’s cloak]

Grandma: "Now go ahead, put it on, dear."

//[player goes through a process to equip the cloak, similar to the process of eating the cake. Click the cloak in inventory, choose “equip” from drop down menu, get flipped to friends tab, see that there is an “equip” button over every character’s portrait. Click the one over the kid’s portrait. Get sent back to inventory page. Yay.]

Grandma: "…"

NAR: Grandma's voice lowers to a hush, and you have to strain to hear the few words you do.

Grandma: "It shouldn't be me doing this. My grandchild...leaving this village, our home, so young...it's too early. I hope this is the right decision..."

NAR: A smirk returns to her face, and her voice fills the air just as before.

Grandma: "You look just like he did, embarrassed face and all! It may seem a bit big still, but you’ll grown into it—I mean, you’ll grow to appreciate its comforts as we travel the wide open plains beyond the village."

    + Child: "We’re going on a trip??"
    ->GrandmaNods
    
    + Child: "...We’re leaving the village?"
    ->GrandmaNods

=== GrandmaNods ===

NAR: Grandma nods.

Grandma: "We’re not just leaving on a trip, little one. No, no, no. We’re doing something much more exciting!"

Grandma: "We’re embarking, on our very. Own. Adventure!"

    + Child: ADVENTURE?!
    Grandma: "Yes! An adventure worthy of a brave hero such as yourself! Finding new places! Meeting new faces!
    Perhaps confronting new foes! And you’re going to love where we’re going, I promise."
    ->Adventure
    
    + Child: "...So, we’re leaving the village?"
    NAR: Grandma gives a wan smile.
    Grandma: "Well… yes. We may be leaving the village for a time, little one. But we get to see places that are exciting, and meet new people, and, I know you’ll love where we’re going."
    ->Adventure
    
    + Child: "...Will we come back home after?"
    Grandma: "...Well, little one, that’s something we can think about once we get where we’re going."
    ->Adventure

=== Adventure ===
    + Child: "Where are we going?"

Grandma: "Somewhere you can look out over the land and see everything—the towns, the mountains, the clouds far below. Where the stars are so close you could nearly reach up and snatch them…"

Grandma: "Where the sunset bathes everything in glowing light, warmer and more brilliant than any other sunset…"

NAR: Grandma looks off into the distance wistfully.

//GRANDMA: *looks up and out, to the very top of the tower of origin, barely visible over the horizon*

Grandma: "The top of the Tower of Origin."

//[camera pans up to the horizon, lingering on the tower, then the sky. Title appears. Wahoo. Now we’re on the travel screen]
-> END

