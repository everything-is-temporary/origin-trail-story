//->CapitalCityGateGuard

=== CapitalCityGateGuard ===
{ CapitalCityGateGuard == 1:
Grandma: “...” //#scared

//Child: “Grandma, are you okay?”

//Child: “Grandma?”

//Child: “Grandma…”

Guard1: “Halt! State your business!”

//Grandma: "..." //#angry #[moves closer to guards] 

Grandma: "What happened here?!"

Guard2: "In case you’re blind--"

Guard1: “This area is not safe for travelers anymore and we advise you to leave now if you can!”

*Child: “So we can’t go in?”
->GettingIn

*Child: “Do we have to go, Grandma?”
->GettingIn

*Child: “Why?”
->GettingIn
-else:
Guard2: "You've been warned..."
-> END
}

= GettingIn
NAR: Grandma stares them down. The lead guard swallows, and you want to do the same.

Grandma: “Now listen here young man, my name is Klavelina Karjal, Higher Member of the Artisan’s Guild, and my family lives here! 

Grandma: I demand to know what is going on!”

Guard1: “I am sorry, but all you need to know is that this area is not safe and we advise you to--!”

Grandma: “If you won’t help, I’ll find someone who will!”

Guard1: “By order of the Magistrates--”

Guard2: “We haven’t heard from the Magistrate in weeks; they’re still boarded up in the gardens.”

Guard1: “But--”

Guard2: “Look, P. I don’t think these people are the kind to be dissuaded. If they want to go in, let them in.”

NAR: The two guards exchange looks, and the lead one relents.

//Guard: #[retreats off screen, gate opens]

Guard2: “P means well, but things are...well--you'll see." 

Guard2: "If possible, conduct your business, get some supplies, and get out. I know that’s what we’ll be doing if it gets any worse.”

Grandma: “What is the matter with you? You are the guard! What’ll happen to the people you’re supposed to protect?” // #aghast

Guard2: “And who will protect us from them? Ah, your eyes will convince you more than any amount of my words will."

Guard2: "P's and my warnings still stand. Good day...”
->END



//->CapitalStatue
=== CapitalStatue ===
NAR: “HERE STANDS VIOLA, SHE WHO WAS TOUCHED BY THE GODDESS AND SWORE TO DEFEND US AGAINST DARKNESS”
->END

//->Innkeep
=== Innkeep ===
{Innkeep == 1:
NAR: You stop to rest your feet in a tavern at the edge of the capital. 

Grandma: "This used to be my favorite spot in town. One of my friends would sing, every night, on that stage."

Grandma: "Everyone would come to listen to her, and we'd all dance..."

NAR: ...It's much quieter than that now. The source of the noise in the tavern seem to be coming from the innkeeper at the bar.

NAR: Two strangely noisy patrons, are slumped over the countertop.

Innkeep: "Enough! I need to cut you two off--it's the middle of the day, for Goddess' sake!"

Rioter1@FunnyMan: "C'mon--it's the revolution! At least make it a good time--"

Rioter2@FunnyWoman: "THE MAGISTRATES CAN GO F-"

NAR: Grandma covers your ears for a bit.

Grandma: "...Hail, innkeeper. What happened to the previous owner of this tavern?" 

Grandma: "I stopped receiving letters from them a few months back. Don't tell me they've finally kicked the bucket."

NAR: The innkeep looks massively relieved to have someone else to talk to.

//make more bitter - deflect onto the grandma
Innkeep: "Sorry, ma'am, I'm afraid I can't tell you. They left a while back…"

Innkeep: "...Along with the rest of the staff…"

Innkeep: "...And, most of the city..."

Innkeep: "So right now, it's my tavern."

Grandma: "Well then, could you tell me where they've gone--"

NAR: Unfortunately, it looks like he's busy wrangling with the two funny people.

NAR: They attempted to hop over the countertop while he wasn't paying attention.

NAR: Grandma sighs.

Grandma: "Come, little one. We need to hurry."
-else:
NAR: The innkeep is still busy dealing with the drunkards.
}

-> END
//->IllicitShopkeep

=== IllicitShopkeep ===
{IllicitShopkeep == 1:
Shopkeep: "Get yer traveling supplies here! Got food, weapons, wagon parts--everythin' ya need to get OUTTA this place!" 

Shopkeep: "Allll sorts'a bits n bobbles, curiosities, magic items!"

Shopkeep: "Get yer rioting gear here! Get caught off guard by the city guard, no longer!"

NAR: A shrewd looking man with a loud voice stands before a marketplace stall nearly bursting at the seams with goods. 

NAR: Just off to the side, a hulking dog lies in wait, lifting its massive head to scrutinize approaching passerby.

NAR: ...You notice that the stalls around this one have been picked clean.

Grandma:"..."  //#angry 

Shopkeep: "Hey there, fine folks! You look like you're from outta town. Headin' very far?" //#smile

Shopkeep: "Now, the Capital ain't as pretty as it normally is..."

Shopkeeper: "But, you can still take a little piece of Capital beauty along with you!"

NAR: He sweeps an arm over his assortment of trinkets.

//#[open shop?]

NAR: Suddenly, a muscular man carrying a crate with a busted top appears from out of the stands.

NAR: He drops the container besides the shopkeeper.

Mook@???: "Here ya're, boss."

NAR: The shopkeep gives him a sharp smile and a clap on the back.

NAR: The other man disappears back into the stands.

Shopkeep: "...Well? You gonna buy anything?"

*CHILD: "Yes please!" 
->answer
*CHILD: "No..." 
->answer
-else:
Grandma: "I said, don't trust people like him."

NAR: She steers you away from the shop.

-> END
}

    = answer
    
    Grandma: "...not likely."
    
    NAR: Grandma begins to pull you along, away from the...'shopkeep.'
    
    Grandma: "Never trust people like him."

    -> END

//->Protestors

=== Protestors ===
{Protestors == 1:
Protestor1@LoudPerson: “Death to the Magistrates! Death to the Magistrates!”

Protestor2@QuieterPerson: “Yeah, r-rats like the Magistrates deserve to be exterminated, l-like, uh, uhh-”

Protestor1@LoudPerson: “What are doing!? This isn’t what we practiced for!” 

NAR: The brash rioter clapped at the timid rioter, egging them on.

Protestor1@LoudPerson: “Out with the softeners! Speak confrontational, NOT conversational!" 

Protestor1@LoudPerson: "NOW SCREAM! SET YOUR ANGER FREE, LASH OUT AT THE WORLD!”

Protestor2@QuieterPerson: “C-COME OUT AND FACE US WITH DIGNITY, COWARDS! BASTARD IDIOT SLIME… PIGS… RHHHAGGGHHH!!”

Protestor1@LoudPerson: “That’s more like it!”

NAR: The rioter pat the other on the back before turning to you.

Protestor1@LoudPerson: “Hey kid, you best go the other way. We’re about to start a real bloodbath..."

Protestor1@LoudPerson:"...and it’d be a shame for a pipsqueak like you to see red, too...”

NAR: They kneeled down and pat your head, giving a curt smile as-

Protestor2@QuieterPerson: “FIGHT ME, YOU FOOLS! AAAGGHHHHHHH!!!” 

NAR: ...as the timid rioter became anything but.
-else:
Protestor2@QuieterPerson: “FIGHT ME, YOU FOOLS! AAAGGHHHHHHH!!!” 
}

-> END


//->Protestors2

=== Protestors2 ===
{Protestors2 == 1:
Protestor3@ScatteredPerson: “I trusted the Magistrates, I trusted them to hold back the Final Night!"

Protestor3@ScatteredPerson: "But instead they sit on their thrones, unmoving and irreverent to us, the people who toil away at their lands!”

Protestor3@ScatteredPerson: “Heroes don’t exist in this world! To hell with them, and all their damned followers!”

Protestor3@ScatteredPerson: “How could our lives be squandered like this!?”

Protestor3@ScatteredPerson: “You understand, right? This is our chance to save the world! For real! 

Protestor3@ScatteredPerson: "The Magistrates, they’re behind it all, we’ll get them! And then, then, then...!!”

NAR: They fall to the floor, despairing, hands hitting the rubble red.

-else:
Protestor3@ScatteredPerson: "Then we'll--we can--they..."

NAR: They continue despairing, lying on the floor.

}

-> END
//->GuardAmbient
=== GuardAmbient ===
NAR: A plethora of voices ring out from the din.

Guard: Shore up the barricade!

Guard2: We just have to buy time for them to figure out a solution...

Guard3: How are our food stores? ...Well, maybe tell them that they can’t have fancy cheeses with every meal!

Guard4: Deepest apologies Madam Kess, but we can’t spare the manpower to search for a dog, no matter his pedigree...

-> END

//->Noble1
=== Noble1 ===

Noble1@PoshNobleman: “Oh, what to do, what to do... I’ve yet to have set off on my morning walk ever since those swine below started swarming our gates...“
NAR: They pace around in circles, stroking their chin in a methodical, regular rhythm.

Noble1@PoshNobleman: “Gah, I knew those heroes were nothing but trouble... wouldn’t run a damned errand as simple as killing a single man... how difficult is that!? Why, I do that all-” 

NAR: He pauses, holds his tongue still, and breathes deeply.

Noble1@PoshNobleman: “...surely you know better behavior than the primates outside, yes? Hoho.” 

-> END

//->NobleGroup //People actually trying to find an answer to the End.
=== NobleGroup ===

Noble2@Councilman: And what is our response?!

Noble3@Lord: Shouting that will not make the rest of us come to an answer any faster!

Noble2@Councilman: So we have none!

Noble3@Lord: No. We are just...at a juncture.

Noble4@Noble: Well, what can we do? The mages and sages all went with Viola...did they leave any notes or clues as to what is happening?

NAR: Raucous laughter can be heard.

Noble3@Lord: Only that it would be not ideal.

Noble2@Councilman: Hopefully the High Lords

The planning session is scheduled to reconvene in approximately ten minutes! 

-> END

//-> Mourner
=== Mourner ===
Mourner: “It was only a matter of time before everything came tumbling down...”

NAR: The sad woman chuckled, before choking up.

Mourner: “Too late for everyone to call for arms... damage is already done. The end of the Magistrates won’t bring back our heroes, our friends, our Earth...” 

Mourner: “Ah, I wish the end would come sooner... the only thing worse than an execution is the waiting cell. Don’t you agree?”
-> END


